# Kako dodati novi proizvod

### 1. Otvori `config.json`

### 2. Otvori editor

![](images/edit.png)

#### 2.1. Klikni na jezicak za biranje tipa radnje

#### 2.2. Izberi edit

#### 2.3. Klikni edit dugme

### 3. Dodaj proizvod

![](images/commit.png)

#### 3.1. Dodaj proizvod

Prozvodi su izlistani u obliku:

```json
"products": {
    "nadimak": "url",
    "nadimak": "url",
    "nadimak": "url"
}
```

**BITNO**: Ako se negde propusti zarez ili doda nakon poslednjeg proizvoda, program nece raditi!

**NE**

```json
"products": {
    "nadimak": "url",
    "nadimak": "url",
    "nadimak": "url"
    "nadimak": "url"
}
```

**NE**

```json
"products": {
    "nadimak": "url",
    "nadimak": "url",
    "nadimak": "url",
    "nadimak": "url",
}
```

**DA**

```json
"products": {
    "nadimak": "url",
    "nadimak": "url",
    "nadimak": "url",
    "nadimak": "url"
}
```

#### 3.2. Sacuvaj promenu klikom na dugme `Commit changes`
